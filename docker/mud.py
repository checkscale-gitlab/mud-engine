#!/usr/bin/env python3
import os
import mud_engine
import logging
import sys
from mud_engine import MUDEngine

logging.basicConfig(level=logging.DEBUG)

example_name = os.environ.get("example", None)
admin_name = os.environ.get("admin", "ben")
port = os.environ.get("port", "5000")

example_class = {
    "quicksand": lambda: __import__('mud_engine.examples.quicksand', fromlist=['']).QuickSandMUD,
    "guardian": lambda: __import__('mud_engine.examples.guardian_npc', fromlist=['']).GuardianMUD,
}.get(example_name, lambda: mud_engine.engine.MUDEngine)()

logging.debug(f"Running {example_class} on port {port}, login as {admin_name} for admin privs")

mud = example_class("0.0.0.0", int(port))
mud.admins.append(admin_name)
mud.run()

sys.exit(0)


